%define SYSCALL_EXIT 60
%define SYSCALL_READ 0
%define SYSCALL_WRITE 1

%define STDIN 0
%define STDOUT 1

%define CHAR_NEWLINE 0xA
%define CHAR_TAB 0x9

%define UINT8_DEC_SIZE 0x21         ; used to allocate single UINT8's chars on stack
%define INT_TO_ASCII_SYMBOL 0x30


section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYSCALL_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; string pointer in rdi
; result in rax
string_length:
    xor rax, rax
.loop:
    cmp byte[rdi+rax], 0
    je .end_string_length
    inc rax
    jmp .loop
.end_string_length:
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; string pointer in rdi
; result in stdout
print_string:
    push rdi
    call string_length
    pop rsi                 ; string pointer
    mov rdx, rax            ; string length
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, CHAR_NEWLINE


; Принимает код символа и выводит его в stdout
; symbol code in rdi
; result in stdout
print_char:
    push rdi
    mov rsi, rsp
    pop rdi
    mov rdx, 1
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT
    syscall
    ret


; Выводит знаковое 8-байтовое число в десятичном формате
; number in rdi
; result in stdout
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; number in rdi
; result in stdout
print_uint:
    mov rax, rdi
    mov rcx, rsp
    sub rsp, UINT8_DEC_SIZE
    mov r9, 10          ; ну тут-то не надо в дефайн, делитель при делении на 10 сложно обозвать как-то понятней, чем 10
    dec rcx
    mov [rcx], byte 0x0
.loop:
    xor rdx, rdx        ; clear rdx (quotient)
    div r9              ; rax / r9 -> rax (result), rdx (quotient)
    add rdx, INT_TO_ASCII_SYMBOL
    dec rcx
    mov [rcx], dl
    test rax, rax
    jnz .loop
    mov rdi, rcx
    call print_string
    add rsp, UINT8_DEC_SIZE
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; first string pointer in rdi
; second string pointer in rsi
; result in rax
string_equals:
    xor rax, rax
    xor rcx, rcx
.loop:
    mov al, [rdi+rcx]
    mov ah, [rsi+rcx]
    cmp al, ah
    jne .false
    inc rcx
    test al, al
    jnz .loop
    mov rax, 1
    ret
.false:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
; symbol in stdin
; result in rax
read_char:
    push 0x0
    mov rsi, rsp    ; destination (on stack)
    mov rdx, 1      ; string length (1 symbol)
    mov rax, SYSCALL_READ
    mov rdi, STDIN
    syscall
    pop rax
    ret

; Проверяет, является ли символ пробельным (0x20, 0x9, 0xA)
; symbol in rdi
; result in rax
is_whitespace:
    cmp rdi, ' '
    je .true
    cmp rdi, CHAR_TAB
    je .true
    cmp rdi, CHAR_NEWLINE
    je .true
.false:
    xor rax, rax
    ret
.true:
    mov rax, 1
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; buffer address in rdi
; buffer length in rsi
; on success: buffer address in rax, word length in rdx
; on fail: 0 in rax
read_word:
    push rdi
    push rsi
    mov rcx, 0
    push rcx
.skip_whitespace:
    call read_char
    test rax, rax
    je .pop_error
    mov rdi, rax
    push rax
    call is_whitespace
    test rax, rax
    pop rax
    jne .skip_whitespace
.loop:
    pop rcx
    pop rsi
    pop rdi
    cmp rcx, rsi
    jge .end_error
    mov [rdi+rcx], rax
    inc rcx
    push rdi
    push rsi
    push rcx
    call read_char
    cmp rax, 0
    je .end_success
    mov rdi, rax
    push rax
    call is_whitespace
    test rax, rax
    pop rax
    je .loop
.end_success:
    pop rcx
    pop rsi
    pop rax
    mov rdx, rcx
    ret
.pop_error:
    pop rdx
    pop rsi
    pop rdi
.end_error:
    xor rax, rax
    ret



; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; string pointer in rdi
; result: number in rax, length in rdx
parse_uint:
    xor rcx, rcx
    xor rax, rax
    xor rdx, rdx
.loop:
    mov dl, byte [rdi+rcx]
    cmp rdx, '0'
    jb .end
    cmp rdx, '9'
    ja .end
    inc rcx
    sub rdx, '0'
    imul rax, 10
    add rax, rdx
    jmp .loop
.end:
    mov rdx, rcx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rdx, rdx
    xor rcx, rcx
    mov dl, byte [rdi+rcx]
    cmp rdx, '-'
    je .negative
    call parse_uint
    ret
.negative:
    inc rdi
    call parse_uint
    test rdx, rdx
    je .end
    inc rdx
    neg rax
.end:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; string pointer in rdi
; buffer pointer in rsi
; buffer length in rdx
; result in rax
string_copy:
    xor rcx, rcx
.loop:
    cmp rcx, rdx
    je .end_error
    mov al, [rdi+rcx]
    mov [rsi+rcx], al
    cmp al, 0
    je .end
    inc rcx
    jmp .loop
.end_error:
    xor rcx, rcx
.end:
    mov rax, rcx
    ret
